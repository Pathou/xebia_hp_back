var Isbn = function () {};

/**
 * Add an book to the cache
 *
 * @param {String} isbn
 * @return {Object}
 * @throw Exception
 */
Isbn.prototype.getInfos = (isbn) => {
  
  var infos = Isbn._getBooks()[isbn];
  
  if(infos == null) throw new Exception('Livre inexistant');
  
  return infos;
};

/**
 * Add an book to the cache
 *
 * @return {Object}
 */
Isbn._getBooks = () => {
  
  return {
    'c8fabf68-8374-48fe-a7ea-a00ccd07afff': {
      'isbn': 'c8fabf68-8374-48fe-a7ea-a00ccd07afff',
      'description': "Le jour de ses onze ans, Harry Potter, un orphelin élevé par un oncle et une tante qui le détestent, voit son existence bouleversée. Un géant vient le chercher pour l'emmener à Poudlard, une école de sorcellerie ! Voler en balai, jeter des sorts, combattre les trolls : Harry Potter se révèle un sorcier doué. Mais un mystère entoure sa naissance et l'effroyable V..., le mage dont personne n'ose prononcer le nom.",
      'author': 'J. K. Rowling',
      'publishDate': '2011',
      'rating': 5
    },
    'a460afed-e5e7-4e39-a39d-c885c05db861': {
      'isbn': 'a460afed-e5e7-4e39-a39d-c885c05db861',
      'description': "Harry Potter fait une rentrée fracassante en voiture volante à l'école des sorciers. Cette deuxième année ne s'annonce pas de tout repos... surtout depuis qu'une étrange malédiction s'est abattue sur les élèves. Entre les cours de potion magique, les matches de Quidditch et les combats de mauvais sorts, Harry et ses amis Ron et Hermione trouveront-ils le temps de percer le mystère de la Chambre des Secrets ?Un livre magique pour sorciers confirmés.",
      'author': 'J. K. Rowling',
      'publishDate': '2011',
      'rating': 4
    },
    'fcd1e6fa-a63f-4f75-9da4-b560020b6acc': {
      'isbn': 'fcd1e6fa-a63f-4f75-9da4-b560020b6acc',
      'description': "Sirius Black, le dangereux criminel qui s'est échappé de la forteresse d'Azkaban, recherche Harry Potter. C'est donc sous bonne garde que l'apprenti sorcier fait sa troisième rentrée à Poudlard. Au programme : des cours de divination, la fabrication d'une potion de ratatinage, le dressage des hippogriffes... Mais Harry est-il vraiment à l'abri du danger qui le menace ?",
      'author': 'J. K. Rowling',
      'publishDate': '2011',
      'rating': 3
    },
    'c30968db-cb1d-442e-ad0f-80e37c077f89': {
      'isbn': 'c30968db-cb1d-442e-ad0f-80e37c077f89',
      'description': "Harry Potter a quatorze ans et entre en quatrième année au collège de Poudlard. Une grande nouvelle attend Harry, Ron et Hermione à leur arrivée : la tenue d'un tournoi de magie exceptionnel entre les plus célèbres écoles de sorcellerie. Déjà les délégations étrangères font leur entrée. Harry se réjouit... Trop vite. Il va se trouver plongé au cœur des évènements les plus dramatiques qu'il ait jamais eu à affronter.",
      'author': 'J. K. Rowling',
      'publishDate': '2007',
      'rating': 5
    },
    '78ee5f25-b84f-45f7-bf33-6c7b30f1b502': {
      'isbn': '78ee5f25-b84f-45f7-bf33-6c7b30f1b502',
      'description': "À quinze ans, Harry entre en cinquième année à Poudlard mais il n'a jamais été aussi anxieux. L'adolescence, la perspective des examens et ces étranges cauchemars... Car Celui-Dont-On-Ne-Doit-Pas-Prononcer-Le-Nom est de retour. Le ministère de la Magie ne semble pas prendre cette menace au sérieux, contrairement à Dumbledore, le directeur du collège de Poudlard. La résistance s'organise alors autour de Harry qui va devoir compter sur le courage et la fidélité de ses amis de toujours...",
      'author': 'J. K. Rowling',
      'publishDate': '2007',
      'rating': 5
    },
    'cef179f2-7cbc-41d6-94ca-ecd23d9f7fd6': {
      'isbn': 'cef179f2-7cbc-41d6-94ca-ecd23d9f7fd6',
      'description': "Dans un monde de plus en plus inquiétant, Harry se prépare à retrouver Ron et Hermione. Bientôt, ce sera la rentrée à Poudlard, avec les autres étudiants de sixième année. Mais pourquoi Dumbledore vient-il en personne chercher Harry chez les Dursley ? Dans quels extraordinaires voyages au cœur de la mémoire va-t-il l'entraîner ?",
      'author': 'J. K. Rowling',
      'publishDate': '2011',
      'rating': 5
    },
    'bbcee412-be64-4a0c-bf1e-315977acd924': {
      'isbn': 'bbcee412-be64-4a0c-bf1e-315977acd924',
      'description': "Cette année, Harry a dix-sept ans et ne retourne pas à Poudlard. Avec Ron et Hermione, il se consacre à la dernière mission confiée par Dumbledore. Mais le Seigneur des Ténèbres règne en maître. Traqués, les trois fidèles amis sont contraints à la clandestinité. D'épreuves en révélations, le courage, le choix et les sacrifices de Harry seront déterminants dans la lutte contre les forces du Mal.",
      'author': 'J. K. Rowling',
      'publishDate': '2011',
      'rating': 4
    },
  };
};

module.exports = new Isbn();