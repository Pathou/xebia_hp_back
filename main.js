var lib = require('./lib/isbn');
var app = require('express')();

// Get infos From ISBN
app.get('/isbn/:isbn', function(req, res) {
  
  // CORS Access-Control-Allow-Origin
  res.setHeader('Access-Control-Allow-Headers', 'x-requested-with, origin, content-type, accept');
  res.setHeader('Access-Control-Allow-Origin', '*');
  
  try {
    var infos = lib.getInfos(req.params.isbn);
  
    res.setHeader('Content-Type', 'text/plain');
    
    // Fake Long treatment
    setTimeout(function() {
      res.end(JSON.stringify(infos));
    }, 2000);
    
  } catch(e) {
    // Book not found
    res.setHeader('Content-Type', 'text/plain');
    res.status(404).send('Page not found');
  }
})
.use(function(req, res, next){
  // Page not found
  res.setHeader('Content-Type', 'text/plain');
  res.status(404).send('Page not found');
})
.listen(1337);